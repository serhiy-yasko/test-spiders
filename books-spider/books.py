# -*- coding: utf-8 -*-
import scrapy


class BooksToScrapeSpider(scrapy.Spider):
    name = 'books-toscrape'
    allowed_domains = ['toscrape.com']
    start_urls = ['http://books.toscrape.com',]
    custom_settings = {
        'AUTOTHROTTLE_ENABLED': True,
        'HTTPCACHE_ENABLED': True,
    }

    def parse(self, response):
        urls = response.css(
            'ul.nav-list > li > ul > li > a::attr(href)').extract()
        for url in urls:
            url = response.urljoin(url)
            yield scrapy.Request(url=url, callback=self.parse_details)

    def parse_details(self, response):
        for book in response.css('article'):
            item = {
                'book_title': book.css('a::attr(title)').extract_first(),
                'book_price': book.css('p.price_color::text').extract_first(),
                'book_image_url': response.urljoin(book.css(
                    'img::attr(src)').extract_first()),
                'book_details_page_url': response.urljoin(book.css(
                    'a::attr(href)').extract_first()),
            }
            yield item
        # follow the pagination links
        next_page_url = response.css('li.next > a::attr(href)').extract_first()
        if next_page_url:
            next_page_url = response.urljoin(next_page_url)
            yield scrapy.Request(url=next_page_url, callback=self.parse_details)